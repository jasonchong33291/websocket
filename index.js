const express = require("express");
require('dotenv').config();
const { createServer } = require("http");
const { Server } = require("socket.io");
const bodyParser = require("body-parser");
const NodeCache = require( "node-cache" );
const myCache = new NodeCache({ checkperiod: 0 });
const { default: axios } = require("axios");
const league_ids = ['429', '40', '39', '41', '42', '61', '62', '78', '79', '2', '3', '94', '95'];
// const league_ids = ['390'];
const app = express();
const httpServer = createServer(app);

const io = new Server(httpServer, { /* options */ 
    maxHttpBufferSize: 1e8,
    cors:{origin:"*"}
});

if(myCache.has( "match" ) === false){
  myCache.set( "match", JSON.stringify([]));
}

io.on("connection", (socket) => {
  // ...
  console.log("The Server is On");

  socket.on("disconnect", (reason) => {
    console.log(`disconnect ${socket.id} due to ${reason}`);
  });

  socket.on("join", (data) => {
    console.log(`${socket.id} had join`);
  });

});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.post('/send', (req, res) => {
    if(req.body.room !== null && req.body.room !== '' && typeof req.body.room !== "undefined") {
        // do something
        io.emit(req.body.room +"_rates", req.body.data);
    }
    else{
        io.emit("rates", req.body.data);
    }
    
    res.set('Content-Type', 'application/json');
    res.send('{status:1}')
})

app.post('/setCache', (req, res) => {
  
  myCache.set( "match", req.body.data);
  res.set('Content-Type', 'application/json');
  res.send('{status:1}');
});

app.get('/getCache', (req, res) => {  
  res.set('Content-Type', 'application/json');
  res.send('{status:'+ myCache.get( "match" ) +'}');
});
app.get('/testlive', (req, res) => {  
  Object.values(league_ids).forEach( val => {
  
    axios.get(process.env.APP_URL + '/odds/live', { 
      headers: { 'x-apisports-key': process.env.APP_KEY },
      params: {
        'league' : val
      }
    })
    .then(response => {
      io.emit( "league_" + val +"_rates", response.data.response);
    })
    .catch((error) => {
        console.log('error ' + error);
    })
  })
  res.set('Content-Type', 'application/json');
  res.send('{status:1}');
})

app.get('/test2',async (req, res) => {  
 Object.values(league_ids).forEach(val => {
    axios.get('http://localhost/test/odds/live/')
    .then(response => {
      
      axios.post('http://127.0.0.1:8000/api/matchdata',{
        data: JSON.stringify(response.data),
        league: val
      })
      .then(response1 => {
        console.log(response1.data.output.response);
        
      })
      .catch((error) => {
        console.log('error111 ' + error);
      })
      
    })
    .catch((error) => {
      console.log('error ' + error);
    })
    
  });
  res.set('Content-Type', 'application/json');
  res.send('{status:1}');
});

async function waitUntil() {
  console.log('inin socket');
  return await new Promise(resolve => {
    const interval = setInterval(() => {
      console.log('inin socket inside loop');
      Object.values(league_ids).forEach( val => {
  
        axios.get(process.env.APP_URL + 'odds/live', { 
          headers: { 'x-apisports-key': process.env.APP_KEY },
          params: {
            'league' : val
          }
        })
        .then(response => {
          axios.post( process.env.MAIN_APP_URL+'api/matchdata',{
            data: JSON.stringify(response.data),
            league: val
          })
          .then(response1 => {
            Object.values(response1.data.output.response).forEach(match => {
              io.emit( "match_" + match.fixture.id +"_rates", JSON.stringify(match));
            });
             io.emit( "league_" + val +"_rates", JSON.stringify(response1.data.output.response));
          })
          .catch((error) => {
            console.log('error: ' + error);
          })
        })
        .catch((error) => {
            console.log('error ' + error);
        })
      })
    }, 100000);
  });
}
const bar = waitUntil()


httpServer.listen(3000);